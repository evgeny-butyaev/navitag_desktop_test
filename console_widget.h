/**
  * @file console_widget.h
  * @author Evgeny V. Zalivochkin
  */

#pragma once
#ifndef CONSOLE_WIDGET_H_INCLUDED
#define CONSOLE_WIDGET_H_INCLUDED

#include <QColor>
#include <QFont>
#include <QKeySequence>
#include <QTextBrowser>

//////////////////////////////////////////////////////////////////////////////////////////////////
/*! \brief Поддержка окна консольного вывода

  На текущий момент окно выполняет следующие функции:
	- "Расцвеченный" по строкам вывод
	- Добавление строк в конец с "умным" скроллиногом
	- Всплывающее окно поиска
*/
class ConsoleWidget : public QTextBrowser
{
	Q_OBJECT

public:
	explicit ConsoleWidget(QWidget* pParent = nullptr);

private:
	class SearchBoxLayout; // Он нужен только внутри, сигналов-слотов нет, так что пусть будет внутренний!

signals:

public slots:
	void find();
	void findNext();
	void findPrevious();
	void findClose();

private slots:
	void onEditFinished();

	// Управление
public:
	void addLine(bool is_html, QString const& strLine, QFont::Weight nWeight, QColor pColor);
	void addLine(QString const& strLine, QFont::Weight nWeight, QColor pColor);
	void addHtml(QString const& html);

	bool searchBoxVisible() const;
	void setSearchBoxVisible(bool bVisible);

private:
	class SearchBox;
	SearchBox* m_pForm;

	// Хелперы
private:
	void connect_sequence(QKeySequence pKeys, const char* strSlot);
};
//////////////////////////////////////////////////////////////////////////////////////////////////

inline void ConsoleWidget::addLine(QString const& strLine, QFont::Weight nWeight, QColor pColor)
{
	addLine(false, strLine, nWeight, pColor);
}

inline void ConsoleWidget::addHtml(QString const& html)
{
	addLine(true, html, QFont::Normal, QColor {});
}

#endif // CONSOLE_WIDGET_H_INCLUDED
