#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QColor>
#include <QGeoPositionInfoSource>
#include <QMainWindow>
#include <QScopedPointer>

#include "navitag_connector/navitag_connector.h"
#include "geo_generator.h"

QT_BEGIN_NAMESPACE
namespace Ui
{
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	MainWindow(QWidget* parent = nullptr);
	~MainWindow();

private:
	void log(QColor color, QString text);

	inline void logI(QString text)
	{
		log(Qt::gray, text);
	}

	inline void logW(QString text)
	{
		log(Qt::yellow, text);
	}

	inline void logE(QString text)
	{
		log(Qt::red, text);
	}

private slots:
	void on_btnClearConsole_clicked();
	void on_btnSettings_clicked();
	void on_btnStart_clicked();
	void on_btnStop_clicked();
	void on_btnSendLocation_clicked();
	void on_btnGenStart_clicked();
	void on_btnGenStop_clicked();

private:
	void initilize();

	void on_connector_state_changed(NavitagConnector::ConnectorState);
	void on_new_position(QGeoPositionInfo);

private:
	QScopedPointer<QGeoPositionInfoSource> gps_;
	NavitagConnector nc_;
	QScopedPointer<GeoGenerator> gg_;

private:
	Ui::MainWindow* ui;
};
#endif // MAINWINDOW_H
