#ifndef SETTINGS_DIALOG_H
#define SETTINGS_DIALOG_H

#include <QDialog>

namespace Ui {
class SettingsDialog;
}

class NavitagConnector;

class SettingsDialog : public QDialog
{
	Q_OBJECT

public:
	SettingsDialog(QWidget* parent, NavitagConnector& nc);
	~SettingsDialog() override;

protected:
	void done(int res) override;

public:
	static void setupDefaults(NavitagConnector& nc);

private:
	NavitagConnector& nc_;
	Ui::SettingsDialog *ui;
};

#endif // SETTINGS_DIALOG_H
