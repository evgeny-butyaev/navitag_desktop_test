syntax = "proto2";

///package TrackingProtocol;

option java_package = "com.navitag";
option java_outer_classname = "TrackerProtocol";



/// Message stucture of version 0
message TrackData
{
	message TrackPointInfo
	{
		message NavInfo
		{
			required int64 captured_timestamp = 1;
			optional double lon_deg = 2;
			optional double lat_deg = 3;
			optional double altitude_m = 4;
			optional float speed_kmh = 5;
			optional float accuracy_m = 6;
			optional float direction_deg = 7;
			optional int32 satellite_count = 8;
			optional LocationProvider location_provider = 9;
		}

		message DeviceInfo
		{
			optional int32 battery_power_mv = 1;
			optional int32 battery_charge_pcnt = 2;
			optional int32 temperature_c = 3;
		}

		required NavInfo nav_info = 1;
		optional DeviceInfo device_info = 2;
	}

	optional string deviceid = 1;
	repeated TrackPointInfo track_point_info = 2;
	optional bool is_ping = 3;
}

enum LocationProvider
{
    GPS = 0;
    Network = 1;
}

/// Input message stucture of version 1
message NavitagClientMessage {
	optional sfixed64 message_id = 1;
	optional Ack ack = 2;
	optional string deviceid = 3;
	optional bool is_ping = 4;
	optional DeviceProfile device_profile = 5;
	repeated TrackData.TrackPointInfo track_point_info = 6;
	optional ChatMessage chat_message = 7;
	optional AssistanceRequestStatus assistance_status = 8;
	optional string client_status = 9;
	optional ChatInfo chat_info = 10;
}

message Ack {optional sfixed64 message_id = 1;}

message DeviceProfile {
	optional bool supports_assistance_requests = 2;
	optional string app_version = 3;
	optional string platform = 4;
}

enum RequestStatus {
	UNANSWERED = 0;
	ACCEPTED = 1;
	REJECTED = 2;
	ACCOMPLISHED = 3;
}

message AssistanceRequestStatus {
	// уникальный (как минимум в пределах TCP-сессии) идентификатор запроса
	optional sfixed64 id = 1;
	optional RequestStatus status = 2;
}

message AssistanceRequest {
	// Unique identifier of request
	optional sfixed64 id = 1;
	// Time of occured event in formatted string
	optional string event_time_formatted = 2;
	// Coordinate of event
	optional double lon_deg = 3;
	optional double lat_deg = 4;
	// String with description of location of event (UTF-8 encoding)
	optional string event_location_address = 5;
	// String with short description of event (UTF-8 encoding)
	optional string event_description = 6;
	// Time of creation request in Dispatch System
	optional int64 request_timestamp = 7;
	// Any HTML-info of event
	optional string event_description_html = 8;
}

message ChatMessage {
	optional sfixed64 id = 1;
	optional int64 time = 2;
	optional string theme = 3;
	optional string body = 4;
	optional sfixed64 request_id_reserved = 5;// reserved link on AssistanceRequest
	optional bytes photo_reserved = 6; // reserved field for exchange photo
	optional string operator_name = 7;
}

message ChatInfo {
	repeated sfixed64 onread_notifications = 1;
}

message RouteInfo {
        // Unique identifier of request
        optional sfixed64 id = 1;

	// delta_lat[0] = latitude/180.0*(2^30) // First value is absolute latitude encoded as number of circle of latitude.
	// delta_lat[n] = (latitude[n]-latitude[n-1])/180.0*(2^30) // vector [n -> n+1]
	repeated sint32 delta_lat = 2 [packed=true];

	// delta_lon[0] = longitude/180.0*(2^30) // First value is absolute longitude encoded as number of meridian.
	// delta_lon[n] = (longitude[n]-longitude[n-1])/180.0*(2^30) // vector [n -> n+1]
	repeated sint32 delta_lon = 3 [packed=true];

	message PointSemanticInfo {
		required uint32 point_index = 1; // Reference on geograpical point in delta_lat[point_index] delta_lon[point_index]
		optional bool waypoint = 2; // If true, then this point is placed on the map by the user manually
		optional string address = 3; // Description
	}

	repeated PointSemanticInfo semantic = 4;
	optional float length_km = 5;
	optional uint64 build_time = 6; // unixtime
	optional uint32 time_en_route = 7; // seconds
}

message ClientStatuses {
	repeated string available_statuses = 1;
}

/// Outgoing message stucture of version 1
message NavitagServerMessage {
	optional sfixed64 message_id = 1;
	optional Ack ack = 2;
	optional AssistanceRequest assistance_request = 3;
	optional ChatMessage chat_message = 4;
	optional RouteInfo route_info = 5;
	optional ClientStatuses client_statuses = 6;
}
