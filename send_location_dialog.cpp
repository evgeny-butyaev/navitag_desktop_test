#include "send_location_dialog.h"
#include "ui_send_location_dialog.h"

#include <QDebug>
#include <QDateTime>

SendLocationDialog::SendLocationDialog(QWidget* parent, QGeoPositionInfo known_gpi)
	: QDialog(parent)
	, saved_gpi_ {known_gpi}
	, ui(new Ui::SendLocationDialog)
{
	ui->setupUi(this);
	setWindowFlag(Qt::WindowContextHelpButtonHint, false);

	if (saved_gpi_.isValid() && saved_gpi_.coordinate().isValid())
	{
		ui->spinLatitude->setValue(saved_gpi_.coordinate().latitude());
		ui->spinLongitude->setValue(saved_gpi_.coordinate().longitude());
		ui->spinAltitude->setValue(saved_gpi_.coordinate().altitude());

		if (saved_gpi_.hasAttribute(QGeoPositionInfo::HorizontalAccuracy))
		{
			ui->checkAccuracy->setChecked(true);
			ui->spinAccuracy->setValue(saved_gpi_.attribute(QGeoPositionInfo::HorizontalAccuracy));
		}

		if (saved_gpi_.hasAttribute(QGeoPositionInfo::GroundSpeed))
		{
			ui->checkSpeed->setChecked(true);
			ui->spinSpeed->setValue(saved_gpi_.attribute(QGeoPositionInfo::GroundSpeed));
		}

		if (saved_gpi_.hasAttribute(QGeoPositionInfo::Direction))
		{
			ui->checkDirection->setChecked(true);
			ui->spinDirection->setValue(saved_gpi_.attribute(QGeoPositionInfo::Direction));
		}
	}
	else
	{
		qDebug() << "Can not detect real geoposition.";
	}
}

SendLocationDialog::~SendLocationDialog()
{
	delete ui;
}

QGeoPositionInfo SendLocationDialog::gpi() const
{
	QGeoPositionInfo res;
	QGeoCoordinate gc;
	if (saved_gpi_.isValid() && saved_gpi_.coordinate().isValid())
	{
		res = saved_gpi_;
		gc = saved_gpi_.coordinate();
	}
	else
	{
		res.setTimestamp(QDateTime::currentDateTime());
	}

	gc.setLatitude(ui->spinLatitude->value());
	gc.setLongitude(ui->spinLongitude->value());
	gc.setAltitude(ui->spinAltitude->value());
	res.setCoordinate(gc);

	if (ui->checkAccuracy->isChecked())
	{
		res.setAttribute(QGeoPositionInfo::HorizontalAccuracy, ui->spinAccuracy->value());
	}
	else
	{
		res.removeAttribute(QGeoPositionInfo::HorizontalAccuracy);
	}

	if (ui->checkSpeed->isChecked())
	{
		res.setAttribute(QGeoPositionInfo::GroundSpeed, ui->spinSpeed->value());
	}
	else
	{
		res.removeAttribute(QGeoPositionInfo::GroundSpeed);
	}

	if (ui->checkDirection->isChecked())
	{
		res.setAttribute(QGeoPositionInfo::Direction, ui->spinDirection->value());
	}
	else
	{
		res.removeAttribute(QGeoPositionInfo::Direction);
	}

	Q_ASSERT(res.isValid());
	Q_ASSERT(res.coordinate().isValid());

	return res;
}
