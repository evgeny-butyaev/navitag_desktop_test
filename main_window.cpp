#include "main_window.h"
#include "./ui_main_window.h"

#include <QDateTime>
#include <QFont>
#include <QMetaObject>
#include <QSettings>

#include "send_location_dialog.h"
#include "settings_dialog.h"

#define MAINWINDOW_GROUP QStringLiteral("Main Window")
#define GEOMETRY_KEY QStringLiteral("Position")
#define STATE_KEY QStringLiteral("State")

MainWindow::MainWindow(QWidget* parent)
	: QMainWindow(parent)
	, nc_ {this}
	, ui(new Ui::MainWindow)
{
	ui->setupUi(this);

	QSettings settings;
	settings.beginGroup(MAINWINDOW_GROUP);
	restoreGeometry(settings.value(GEOMETRY_KEY).toByteArray());
	restoreState(settings.value(STATE_KEY).toByteArray());
	settings.endGroup();

	connect(&nc_, &NavitagConnector::stateChanged, this, &MainWindow::on_connector_state_changed);

	QMetaObject::invokeMethod(this, &MainWindow::initilize, Qt::QueuedConnection);
}

MainWindow::~MainWindow()
{
	QSettings settings;
	settings.beginGroup(MAINWINDOW_GROUP);
	settings.setValue(GEOMETRY_KEY, saveGeometry());
	settings.setValue(STATE_KEY, saveState());
	settings.endGroup();

	delete ui;
}

void MainWindow::log(QColor color, QString text)
{
	QMetaObject::invokeMethod(
		this, [c = ui->console, color, text]() {
			c->addLine(QDateTime::currentDateTime().toString("yyyy.MM.dd hh:mm:ss.zzz: ") + text, QFont::Bold, color);
		},
		Qt::QueuedConnection);
}

void MainWindow::initilize()
{
	gps_.reset(QGeoPositionInfoSource::createDefaultSource(this));
	if (gps_)
	{
		logI(tr("Using QGeoPositionInfoSource: %1").arg(gps_->sourceName()));
	}
	else
	{
		logW("QGeoPositionInfoSource is not found.");
	}

	SettingsDialog::setupDefaults(nc_);
}

void MainWindow::on_btnClearConsole_clicked()
{
	ui->console->clear();
}

void MainWindow::on_btnSettings_clicked()
{
	SettingsDialog {this, nc_}.exec();
}

void MainWindow::on_btnStart_clicked()
{
	nc_.start();
}

void MainWindow::on_btnStop_clicked()
{
	nc_.stop();
}

void MainWindow::on_connector_state_changed(NavitagConnector::ConnectorState)
{
	logI(QStringLiteral("Connector state changed to: %1").arg(nc_.stateString()));
}

void MainWindow::on_btnSendLocation_clicked()
{
	SendLocationDialog sld {this, gps_ ? gps_->lastKnownPosition() : QGeoPositionInfo {}};

	if (QDialog::Accepted != sld.exec())
	{
		return;
	}

	nc_.enqueuePosition(sld.gpi());
}

void MainWindow::on_btnGenStart_clicked()
{
	if (gg_)
	{
		logW(QStringLiteral("Generator already running."));
		return;
	}
	//	, start_lat_ {55.971223}
	//	, start_long_ {37.413412}

	gg_.reset(new GeoGenerator(this,
							   55.971223, 37.413412, // Аэропорт Шерематьего
							   120.0, 1.1, 2000));
	connect(gg_.data(), &GeoGenerator::newPosition, this, &MainWindow::on_new_position);
	//	connect(gg_.data(), &GeoGenerator::newPosition, &nc_, &NavitagConnector::enqueuePosition);
	gg_->start();

	logI(QStringLiteral("Generator started."));
}

void MainWindow::on_btnGenStop_clicked()
{
	if (!gg_)
	{
		logW(QStringLiteral("Generator is not running."));
		return;
	}

	gg_.reset();
	logI(QStringLiteral("Generator stopped."));
}

void MainWindow::on_new_position(QGeoPositionInfo gpi)
{
	if ((!gpi.isValid()) || (!gpi.coordinate().isValid()))
	{
		logE(QStringLiteral("Invalid position generated."));
		return;
	}

	logI(QStringLiteral("New position generated: %1 %2").arg(gpi.timestamp().toString(), gpi.coordinate().toString()));
	nc_.enqueuePosition(gpi);
}
