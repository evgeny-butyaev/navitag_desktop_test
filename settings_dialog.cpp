#include "settings_dialog.h"
#include "ui_settings_dialog.h"

#include <QSettings>

#include "navitag_connector/navitag_connector.h"

#define NAVITAG_GROUP QStringLiteral("Navitag")
#define HOST_KEY QStringLiteral("Host")
#define PORT_KEY QStringLiteral("Port")
#define DEVICE_ID_KEY QStringLiteral("Device ID")
#define PLATFORM_KEY QStringLiteral("Platform")
#define VERSION_KEY QStringLiteral("Version")
#define WAIT_RESPONSE_KEY QStringLiteral("Wait Response Timeout")
#define RECONNECT_INTERVAL_KEY QStringLiteral("Reconnect Interval")
#define PING_INTERVAL_KEY QStringLiteral("Ping Interval")
#define MAX_QUEUE_SIZE_KEY QStringLiteral("Maximum Elements In Queue")
#define MAX_PACKET_SIZE_KEY QStringLiteral("Maximum Elements In Packet")

#define HOST_DEFAULT_VALUE QStringLiteral("193.232.47.4")
#define PORT_DEFAULT_VALUE 30159
#define DEVICE_ID_DEFAULT_VALUE QStringLiteral("6RLWW5S8C6V9")
#define PLATFORM_DEFAULT_VALUE QStringLiteral("Cross-platform")
#define VERSION_DEFAULT_VALUE QStringLiteral("0.0.0.1")

SettingsDialog::SettingsDialog(QWidget* parent, NavitagConnector& nc)
	: QDialog {parent}
	, nc_ {nc}
	, ui {new Ui::SettingsDialog}
{
	ui->setupUi(this);
	setWindowFlag(Qt::WindowContextHelpButtonHint, false);

	ui->editHost->setText(nc_.host());
	ui->spinPort->setValue(nc_.port());
	ui->editDeviceID->setText(nc_.deviceID());
	ui->editPlatform->setText(nc_.platform());
	ui->editVersion->setText(nc_.version());
	ui->spinWaitResponse->setValue(nc_.waitResponseTimeout());
	ui->spinReconnectInterval->setValue(nc_.reconnectInterval());
	ui->spinPingInterval->setValue(nc_.pingInterval());
	ui->spinMaxQueueSize->setValue(static_cast<int>(nc_.maxQueueSize()));
	ui->spinMaxPacketSize->setValue(static_cast<int>(nc_.maxLocationsInPacket()));
}

SettingsDialog::~SettingsDialog()
{
	delete ui;
}

void SettingsDialog::done(int res)
{
	QDialog::done(res);
	if (QDialog::Accepted == res)
	{
		nc_.setHost(ui->editHost->text());
		nc_.setPort(static_cast<quint16>(ui->spinPort->value()));
		nc_.setDeviceID(ui->editDeviceID->text());
		nc_.setPlatform(ui->editPlatform->text());
		nc_.setVersion(ui->editVersion->text());
		nc_.setWaitResponseTimeout(static_cast<quint32>(ui->spinWaitResponse->value()));
		nc_.setReconnectInterval(static_cast<quint32>(ui->spinReconnectInterval->value()));
		nc_.setPingInterval(static_cast<quint32>(ui->spinPingInterval->value()));
		nc_.setMaxQueueSize(static_cast<std::size_t>(ui->spinMaxQueueSize->value()));
		nc_.setMaxLocationsInPacket(static_cast<std::size_t>(ui->spinMaxPacketSize->value()));

		QSettings settings;
		settings.beginGroup(NAVITAG_GROUP);
		settings.setValue(HOST_KEY, nc_.host());
		settings.setValue(PORT_KEY, nc_.port());
		settings.setValue(DEVICE_ID_KEY, nc_.deviceID());
		settings.setValue(PLATFORM_KEY, nc_.platform());
		settings.setValue(VERSION_KEY, nc_.version());
		settings.setValue(WAIT_RESPONSE_KEY, nc_.waitResponseTimeout());
		settings.setValue(RECONNECT_INTERVAL_KEY, nc_.reconnectInterval());
		settings.setValue(PING_INTERVAL_KEY, nc_.pingInterval());
		settings.setValue(MAX_QUEUE_SIZE_KEY, static_cast<int>(nc_.maxQueueSize()));
		settings.setValue(MAX_PACKET_SIZE_KEY, static_cast<int>(nc_.maxLocationsInPacket()));
	}
}

void SettingsDialog::setupDefaults(NavitagConnector& nc)
{
	QSettings settings;
	settings.beginGroup(NAVITAG_GROUP);

	nc.setHost(settings.value(HOST_KEY, HOST_DEFAULT_VALUE).toString());
	nc.setPort(static_cast<quint16>(settings.value(PORT_KEY, PORT_DEFAULT_VALUE).toUInt()));
	nc.setDeviceID(settings.value(DEVICE_ID_KEY, DEVICE_ID_DEFAULT_VALUE).toString());
	nc.setPlatform(settings.value(PLATFORM_KEY, PLATFORM_DEFAULT_VALUE).toString());
	nc.setVersion(settings.value(VERSION_KEY, VERSION_DEFAULT_VALUE).toString());
	nc.setWaitResponseTimeout(static_cast<quint32>(settings.value(WAIT_RESPONSE_KEY, nc.waitResponseTimeout()).toUInt()));
	nc.setReconnectInterval(static_cast<quint32>(settings.value(RECONNECT_INTERVAL_KEY, nc.reconnectInterval()).toUInt()));
	nc.setPingInterval(static_cast<quint32>(settings.value(PING_INTERVAL_KEY, nc.pingInterval()).toUInt()));
	nc.setMaxQueueSize(static_cast<std::size_t>(settings.value(MAX_QUEUE_SIZE_KEY,
															   static_cast<int>(nc.maxQueueSize()))
													.toUInt()));
	nc.setMaxLocationsInPacket(static_cast<std::size_t>(settings.value(MAX_PACKET_SIZE_KEY,
																	   static_cast<int>(nc.maxLocationsInPacket()))
															.toUInt()));
}
