#ifndef GEOGENERATOR_H
#define GEOGENERATOR_H

#include <QObject>
#include <QGeoPositionInfo>
#include <QTimer>

class GeoGenerator : public QObject
{
	Q_OBJECT
public:
	GeoGenerator(QObject *parent,
				 // Стартовые координаты
				 double start_lat, double start_long, double start_alt,
				 // Отношение частот по Lat и Lon. Если больше 1, быстрее движемся по Lon
				 double freq_diff,
				 // Через сколько миллисекунд давать следующую координату
				 unsigned interval);

public slots:
	void start();

signals:
	void newPosition(QGeoPositionInfo);

private:
	void on_timer_timeout();
	QGeoCoordinate get_coordunate(quint64 index);

private:
	double start_lat_;
	double start_long_;
	double start_alt_;
	double freq_diff_;
	unsigned interval_;
	qint64 index_;
	QTimer timer_;
};

#endif // GEOGENERATOR_H
