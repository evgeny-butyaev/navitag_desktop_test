#ifndef SEND_LOCATION_DIALOG_H
#define SEND_LOCATION_DIALOG_H

#include <QDialog>
#include <QGeoPositionInfo>

namespace Ui {
class SendLocationDialog;
}

class SendLocationDialog : public QDialog
{
	Q_OBJECT

public:
	explicit SendLocationDialog(QWidget* parent, QGeoPositionInfo known_gpi);
	~SendLocationDialog();

	QGeoPositionInfo gpi() const;

private:
	QGeoPositionInfo saved_gpi_;

private:
	Ui::SendLocationDialog *ui;
};

#endif // SEND_LOCATION_DIALOG_H
